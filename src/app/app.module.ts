import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ElderComponent } from './entities/elder/elder.component';
import { AssetComponent } from './entities/asset/asset.component';
import { SpellComponent } from './entities/spell/spell.component';
import { ConditionComponent } from './entities/condition/condition.component';
import { RareAssetComponent } from './entities/rare-asset/rare-asset.component';
import { ArtifactComponent } from './entities/artifact/artifact.component';
import { CoultistComponent } from './entities/coultist/coultist.component';
import { DetectiveComponent } from './entities/detective/detective.component';
import { MainComponent } from './screen/main/main.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DetectiveComponent,
    ElderComponent,
    AssetComponent,
    SpellComponent,
    ConditionComponent,
    RareAssetComponent,
    ArtifactComponent,
    CoultistComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
