import {Cultist} from '../coultist/coultist';

export class Elder {
  id: number;
  name: string;
  questsToWin: number;
  despair: number;
  ability: string;
  description: string;
  mythDeckStage1: [][];
  mythDeckStage2: [][];
  mythDeckStage3: [][];
  coultist: Cultist;
  backAbility: string;
  backCoultist: Cultist;
}
