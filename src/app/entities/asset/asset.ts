export class Asset {
  id: number;
  name: string;
  type: string;
  effect: string;
  revenge: boolean;
  revengeText: string;
}
