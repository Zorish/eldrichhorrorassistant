export class Condition {
  id: number;
  name: string;
  type: string;
  effect: string;
  sideEffect: string;
  revenge: boolean;
  revengeText: string;
}
