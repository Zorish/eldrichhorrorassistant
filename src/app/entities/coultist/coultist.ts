export class Cultist {
  id: number;
  name: string;
  horror: number;
  strength: number;
  stamina: number;
  ability: string;
}
