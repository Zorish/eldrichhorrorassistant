import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoultistComponent } from './coultist.component';

describe('CoultistComponent', () => {
  let component: CoultistComponent;
  let fixture: ComponentFixture<CoultistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoultistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoultistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
