export class Artifact {
  id: number;
  name: string;
  type: string;
  effect: string;
  revenge: boolean;
  revengeText: string;
}
