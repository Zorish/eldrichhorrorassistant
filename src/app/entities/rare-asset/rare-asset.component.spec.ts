import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RareAssetComponent } from './rare-asset.component';

describe('RareAssetComponent', () => {
  let component: RareAssetComponent;
  let fixture: ComponentFixture<RareAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RareAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RareAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
