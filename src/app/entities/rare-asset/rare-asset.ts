export class RareAsset {
  id: number;
  name: string;
  type: string;
  effect: string;
  sideEffect: string;
  revenge: boolean;
  revengeText: string;
}
