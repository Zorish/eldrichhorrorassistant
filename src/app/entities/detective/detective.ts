import {Asset} from '../asset/asset';
import {Spell} from '../spell/spell';
import {Condition} from '../condition/condition';
import {RareAsset} from '../rare-asset/rare-asset';
import {Artifact} from '../artifact/artifact';

export class Detective {
  id: number;
  name: string;
  class: string;
  physicalHealth: number;
  mentalHealth: number;
  lore: number;
  influence: number;
  observation: number;
  strength: number;
  will: number;
  ability: string;
  action: string;
  startLocation: string;
  focusTokenCount: number;
  clueTokenCount: number;
  physicalDeathText: string;
  mentalDeathText: string;
  prehistory: string;
  photo: any;
  assets: Asset[];
  rareAssets: RareAsset[];
  artifacts: Artifact[];
  spells: Spell[];
  conditions: Condition[];
}
